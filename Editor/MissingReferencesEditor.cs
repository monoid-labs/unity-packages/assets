using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;

namespace Monoid.Unity.Assets {

  public sealed class MissingReferencesEditor : EditorWindow {

    static string[] packages = { "Assets" };

    static bool reload = true;

    static void UpdatePackages() {
      if (!reload) {
        return;
      }
      reload = false;

      var list = new List<string>();
      list.Add("Assets");

      ListRequest request = Client.List(true);//, false);
      do { } while (!request.IsCompleted);

      if (request.Error != null) {
        Debug.LogError($"Could not load (cached) packages [code={request.Error.errorCode}]: {request.Error.message}");
        packages = list.ToArray();
        return;
      }

      foreach (var pkg in request.Result) {
        if (pkg == null) {
          continue;
        }
        bool use = pkg.source == PackageSource.Embedded;
        if (!use) {
          continue;
        }
        list.Add(pkg.assetPath);
      }
      packages = list.ToArray();

      if (packages.Length >= 32) {
        Debug.LogWarning("Only up to 31 embedded packages are supported");
      }
    }

    static string[] FilterPackages(int mask) {
      var folders = new List<string>();
      for (int i = 0; i < 32; ++i) {
        if (i >= packages.Length) {
          break;
        }
        if (((mask >> i) & 1) == 0) {
          continue;
        }
        if (packages[i]?.Length == 0) {
          continue;
        }
        folders.Add(packages[i]);
      }
      return folders.ToArray();
    }


    [MenuItem("Window/Assets/Find Missing References", priority = 3010)]
    static void Init() {
      var window = EditorWindow.GetWindow<MissingReferencesEditor>();
      window.titleContent.text = "Missing References";
      window.Show();
    }

    [SerializeField] string query = "t:Prefab t:ScriptableObject";
    [SerializeField] Object[] objects;
    Vector2 scrollPosition;
    int mask = 1;


    void OnGUI() {
      UpdatePackages();

      string[] folders = FilterPackages(mask);

      EditorGUILayout.BeginHorizontal();
      query = EditorGUILayout.TextField("Search", query);
      string[] assets = folders.Length == 0 ? new string[0] : AssetDatabase.FindAssets(query, folders);
      GUI.enabled = false;
      EditorGUILayout.IntField(assets.Length, new GUIStyle() { alignment = TextAnchor.MiddleCenter }, GUILayout.ExpandWidth(false), GUILayout.MaxWidth(60.0f));
      GUI.enabled = true;
      EditorGUILayout.EndHorizontal();

      mask = EditorGUILayout.MaskField("Folders", mask, packages);

      GUI.enabled = assets.Length > 0;
      if (GUILayout.Button("Find Missing References")) {
        for (int i = 0, n = assets.Length; i < n; ++i) {
          assets[i] = AssetDatabase.GUIDToAssetPath(assets[i]);
        }
        objects = FindObjects(assets);
      }
      GUI.enabled = true;

      if (objects == null) {
        return;
      }

      EditorGUILayout.Separator();

      GUI.enabled = objects.Length > 0;
      if (GUILayout.Button("Select")) {
        Selection.objects = objects;
      }
      GUI.enabled = true;

      scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
      var obj = new SerializedObject(this);
      SerializedProperty prop = obj.FindProperty("objects");
      EditorGUILayout.PropertyField(prop, true);
      EditorGUILayout.EndScrollView();
    }

    static Object[] FindObjects(string[] assetPaths) {
      var ids = new HashSet<int>();
      void Missing(string error, Object obj) {
        Debug.LogError(error, obj);
        ids.Add(obj.GetInstanceID());
      }

      var missingRefs = new MissingReferences(Missing);
      foreach (string assetPath in assetPaths) {
        if (AssetDatabase.GetMainAssetTypeAtPath(assetPath) == typeof(SceneAsset)) {
          continue;
        }
        Object[] assets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
        foreach (var asset in assets) {
          if (!asset) {
            continue; // NOTE(micha): Missing Scripts, ..
          }
          missingRefs.Find(asset, true);
        }
      }

      var objects = new Object[ids.Count];
      int index = 0;
      foreach (var id in ids) {
        Object obj = EditorUtility.InstanceIDToObject(id);
        objects[index++] = obj;
      }

      return objects;
    }



  }

}
