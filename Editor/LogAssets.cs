using UnityEditor;
using UnityEngine;

namespace Monoid.Unity.Assets {

  static class LogAssets {

    public static void Info(Object asset) {
      string path = AssetDatabase.GetAssetPath(asset);
      Debug.Log(path, asset);
    }

    public static void Info(params Object[] assets) {
      foreach (Object asset in assets) {
        Info(asset);
      }
    }

  }

}
