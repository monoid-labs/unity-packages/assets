using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEditor;
using System.Reflection;

namespace Monoid.Unity.Assets {

  public sealed class MissingReferences {

    static bool IsValidAsset(Object obj) {
      if (!obj) {
        return false;
      }
      if (obj is SceneAsset) {
        return false; // prevent error "Do not use readobjectthreaded on scene objects!"
      }
      if ((obj.hideFlags & HideFlags.NotEditable) == HideFlags.NotEditable) {
        return false;
      }
      return true;
    }

    static string ComponentPath(Object obj) {
      Component comp = obj as Component;
      if (!comp) {
        return string.Empty;
      }
      return TransformPath(comp.transform);
    }

    static string TransformPath(Transform t) {
      string name = string.Empty;
      if (t.parent) {
        name = t.name;
        if (t.parent.parent) {
          name = $"{TransformPath(t.parent)}/{name}";
        }
      }
      return name;
    }

    readonly Action<string, Object> action;
    readonly HashSet<int> check = new HashSet<int>();
    readonly List<int> todo = new List<int>();
    readonly List<string> fields = new List<string>();

    public MissingReferences(Action<string, Object> action) {
      this.action = action ?? throw new NullReferenceException("Action cannot be null");
    }

    public void Reset() {
      check.Clear();
      todo.Clear();
      fields.Clear();
    }

    public int Find(Object obj, bool followReferences = false) {
      int n = 0;
      Collect(obj);
      if (fields.Count > 0) {
        n += fields.Count;
        string path = AssetDatabase.GetAssetPath(obj);
        string cases = string.Join("\n", fields.Select(s => "\t" + s));
        try {
          action($"Missing references ({fields.Count}) at {path}/{ComponentPath(obj)}[{obj.GetType().Name}]\n{cases}", obj);
        } catch (Exception e) {
          Debug.LogException(e, obj);
        }
      }
      if (followReferences) {
        while (todo.Count > 0) {
          int i = todo.Count - 1;
          Object next = EditorUtility.InstanceIDToObject(todo[i]);
          todo.RemoveAt(i);
          fields.Clear();
          n += Find(next, followReferences);
        }
        if (todo.Count == 0) {
          fields.Clear();
        }
      } else {
        todo.Clear();
        fields.Clear();
      }
      return n;
    }

    void Collect(object obj, string path = "") {
      if (obj == null || ((obj is Object nObj) && !nObj)) { //NOTE(micha): second half is necessary for `AnimationClip` referred in structs
        fields.Add(path);
        return;
      }

      bool root = path.Length == 0;

      if (!root && obj is Object uObj) {
        string assetPath = AssetDatabase.GetAssetPath(uObj);
        if (assetPath.Length > 0) {
          if (IsValidAsset(uObj)) {
            int id = uObj.GetInstanceID();
            if (check.Contains(id)) {
              return;
            }
            check.Add(id);
            todo.Add(id);
          }
        }
        return;
      }

      var type = obj.GetType();

      if (type.IsPrimitive) {
        return;
      }
      if (type.IsEnum) {
        return;
      }

      if (!(type.IsSerializable || obj is UnityEngine.Object)) {
        return;
      }

      if (type.IsArray) {
        int index = 0;
        foreach (System.Array elem in (System.Array)obj) {
          Collect(elem, $"{path}[{index++}]");
        }
        return;
      }

      if (root || type.IsValueType) {
        foreach (FieldInfo field in type.GetFields()) {
          if (field.IsStatic) {
            continue;
          }
          if (!field.IsPublic && !System.Attribute.IsDefined(field, typeof(UnityEngine.SerializeField))) {
            continue;
          }
          Collect(field.GetValue(obj), $"{path}.{field.Name}");
        }
        return;
      }

    }

  }

}
