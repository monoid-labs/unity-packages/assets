﻿using System;
using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(EnumFlagsAttribute))]
  public sealed class EnumFlagsDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      Enum target = (Enum)Enum.ToObject(fieldInfo.FieldType, property.intValue);
      Type type = target.GetType();

      EditorGUI.BeginProperty(position, label, property);
      EditorGUI.BeginChangeCheck();
      Enum newValue = EditorGUI.EnumFlagsField(position, type.Name, target);
      if (EditorGUI.EndChangeCheck()) {
        property.intValue = (int)Convert.ChangeType(newValue, type);
      }
      EditorGUI.EndProperty();
    }
  }

}
