using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  public sealed class ApplyRevertGUI {

    public interface IEditor {
      bool IsDirty();

      void Apply();
      void Revert();

      void Repaint();
    }

    public ApplyRevertGUI(IEditor editor) => this.editor = editor;

    public void Draw(bool valid) {
      EditorGUILayout.Space();
      using (new EditorGUILayout.HorizontalScope()) {
        GUILayout.FlexibleSpace();

        if (OnApplyRevertGUI()) {
          editor.Repaint();
        }
      }
      EditorGUILayout.Space();

      bool OnApplyRevertGUI() {
        using (new EditorGUI.DisabledScope(!editor.IsDirty())) {
          RevertButton();
          using (new EditorGUI.DisabledScope(!valid)) {
            return ApplyButton();
          }
        }
      }

      bool ApplyButton() {
        if (!GUILayout.Button(Styles.ApplyButton)) {
          return false;
        }
        GUI.FocusControl(null);
        editor.Apply();
        return true;
      }

      void RevertButton() {
        if (!GUILayout.Button(Styles.RevertButton)) {
          return;
        }
        GUI.FocusControl(null);
        editor.Revert();
        if (editor.IsDirty()) {
          Debug.LogError("Modification persists after reset.");
        }
      }
    }

    public void ShowModificationsPopup(bool valid) {
      if (valid) {
        if (EditorUtility.DisplayDialog(Styles.UnappliedModificationsTitle, Styles.UnappliedModificationsText,
                                                            Styles.RevertButton, Styles.ApplyButton)) {
          editor.Revert();
        } else {
          editor.Apply();
        }
      } else if (EditorUtility.DisplayDialog(Styles.UnappliedModificationsTitle, Styles.UnappliedModificationsText,
                                      Styles.RevertButton)) {
        editor.Revert();
      }
    }

    public bool ShowCancelableModificationsPopup(bool valid) {
      if (valid) {
        int userChoice = EditorUtility.DisplayDialogComplex(Styles.UnappliedModificationsTitle, Styles.UnappliedModificationsText,
                                                            Styles.ApplyButton, Styles.CancelButton, Styles.RevertButton);
        switch (userChoice) {
          case 0:
            editor.Apply();
            return true;
          case 2:
            editor.Revert();
            return true;
        }
        return false;
      }
      if (EditorUtility.DisplayDialog(Styles.UnappliedModificationsTitle, Styles.UnappliedModificationsText,
                                      Styles.RevertButton, Styles.CancelButton)) {
        editor.Revert();
        return true;
      }
      return false;
    }

    readonly IEditor editor;

    static class Styles {
      public static readonly string ApplyButton = "Apply";
      public static readonly string RevertButton = "Revert";
      public static readonly string CancelButton = "Cancel";

      public static readonly string UnappliedModificationsTitle = "Unapplied modifications";
      public static readonly string UnappliedModificationsText = "Unapplied modifications. Proceed with care!";
    }
  }

}
