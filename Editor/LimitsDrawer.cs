using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(LimitsAttribute))]
  public sealed class LimitsDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      // NOTE(micha): to lazy to propertly test / support multi-editing
      if (property.serializedObject.isEditingMultipleObjects) {
        EditorGUI.PropertyField(position, property, label, true);
        return;
      }

      LimitsAttribute limits = (LimitsAttribute)attribute;

      SerializedProperty fromField = property.FindPropertyRelative("from");
      SerializedProperty toField = property.FindPropertyRelative("to");
      // vector support
      if (fromField == null) {
        fromField = property.FindPropertyRelative("x");
      }
      if (toField == null) {
        toField = property.FindPropertyRelative("y");
      }

      float fromValue = fromField.floatValue;
      float toValue = toField.floatValue;

      float PADDING = 4 * EditorGUIUtility.pixelsPerPoint;
      float FIELD = EditorGUIUtility.fieldWidth;

      EditorGUI.BeginChangeCheck();
      label = EditorGUI.BeginProperty(position, label, property);
      position = EditorGUI.PrefixLabel(position, label);

      int indent = EditorGUI.indentLevel;
      EditorGUI.indentLevel = 0;
      { // from
        Rect fromPos = position;
        fromPos.width = FIELD;
        fromValue = EditorGUI.FloatField(fromPos, fromValue);
        if (limits.from < limits.to) {
          fromValue = Mathf.Clamp(fromValue, limits.from, toValue);
        } else {
          fromValue = Mathf.Clamp(fromValue, toValue, limits.from);
        }
      }
      { // slider
        Rect sliderPos = position;
        sliderPos.x += FIELD + PADDING;
        sliderPos.width -= 2 * FIELD + 2 * PADDING;
        if (limits.from == limits.to) { // singularity
          bool enabled = GUI.enabled;
          GUI.enabled = false;
          float min = 0, max = 1;
          EditorGUI.MinMaxSlider(sliderPos, ref min, ref max, 0, 1);
          GUI.enabled = enabled;
        } else { // handles from > to well
          float min = Mathf.InverseLerp(limits.from, limits.to, fromValue);
          float max = Mathf.InverseLerp(limits.from, limits.to, toValue);
          EditorGUI.MinMaxSlider(sliderPos, ref min, ref max, 0, 1);
          fromValue = Mathf.Lerp(limits.from, limits.to, min);
          toValue = Mathf.Lerp(limits.from, limits.to, max);
        }
      }
      { // to
        Rect toPos = position;
        toPos = position;
        toPos.x += toPos.width - FIELD;
        toPos.width = FIELD;
        toValue = EditorGUI.FloatField(toPos, toValue);
        if (limits.from < limits.to) {
          toValue = Mathf.Clamp(toValue, fromValue, limits.to);
        } else {
          toValue = Mathf.Clamp(toValue, limits.to, fromValue);
        }
      }
      EditorGUI.indentLevel = indent;
      EditorGUI.EndProperty();

      bool changed = EditorGUI.EndChangeCheck();
      changed |= fromValue != fromField.floatValue;
      changed |= toValue != toField.floatValue;

      if (!changed) {
        return;
      }

      fromField.floatValue = fromValue;
      toField.floatValue = toValue;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
      return property.serializedObject.isEditingMultipleObjects
           ? EditorGUI.GetPropertyHeight(property, true)
           : EditorGUIUtility.singleLineHeight;
    }
  }
}
