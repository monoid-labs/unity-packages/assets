
using System;
using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Assets {

  // multi-edit support via on Unity's `SerializedObject`
  public abstract class DataEditor<Model, View> : ScriptedImporterEditor
#if !UNITY_2019_1_OR_NEWER
                                                , IDisposable
#endif
                                                  where Model : ScriptedImporter
                                                  where View : ScriptableObject, DataEditor<Model, View>.IView {

    #region IView

    public interface IView {
      string Load(Model target);
      string Save(Model target);
    }

    #endregion

#if UNITY_2019_2_OR_NEWER

    #region ScriptedImporterEditor

    protected override Type extraDataType => typeof(View);

    protected override void InitializeExtraDataInstance(UnityEngine.Object extraTarget, int targetIndex) {
      View view = (View) extraTarget;
      Model model = (Model) targets[targetIndex];
      try {
        string err = view.Load(model);
        if (err.Length > 0) {
          Debug.LogError(err, model);
        }
      } catch (Exception e) {
        Debug.LogException(e, model);
      }
    }

    protected override void Apply() {
      if (base.assetTarget == null) {
        return;
      }

      UnityEngine.Object[] targets = base.targets;
      UnityEngine.Object[] extraTargets = base.extraDataTargets;
      int n = Math.Min(targets.Length, extraTargets.Length);
      for(int i=0; i<n; i++) {
        View view = (View) extraDataTargets[i];
        Model model = (Model) targets[i];
        try {
          string err = view.Save(model);
          if (err.Length > 0) {
            Debug.LogError(err, model);
          }
        } catch (Exception e) {
          Debug.LogException(e, model);
        }
      }
    }

    public override void OnInspectorGUI() {
      this.DrawModel();
      this.DrawSeparator();
      this.DrawView();

      this.ApplyRevertGUI();
    }

    protected virtual void DrawModel() => DrawSerializedObject(base.serializedObject, false);

    protected virtual void DrawSeparator() => EditorGUILayout.Separator();

    protected virtual void DrawView() => DrawSerializedObject(base.extraDataSerializedObject, true);

    protected static void DrawSerializedObject(SerializedObject obj, bool skipScript) {
      obj.Update();

      using (var properties = new EditorGUI.ChangeCheckScope()) {
        SerializedProperty property = obj.GetIterator();
        bool enterChildren = true;
        if (skipScript) {
          enterChildren = property.NextVisible(enterChildren); // skip 'script' field
        }
        while (property.NextVisible(enterChildren)) {
          enterChildren = EditorGUILayout.PropertyField(property);
        }

        if (properties.changed) {
          obj.ApplyModifiedProperties();
        }
      }

    }


    #endregion

#else

    #region ScriptedImporterEditor

    public override bool HasModified() {
      if (base.HasModified()) {
        return true;
      }
      if (this.renderer != null) {
        return this.renderer.hasModifiedProperties;
      }
      return false;
    }

    protected override bool OnApplyRevertGUI() {
      using (new EditorGUI.DisabledScope(!this.HasModified())) {
        this.RevertButton();
        return this.ApplyButton();
      }
    }

    protected override void Apply() {
      base.Apply();
      this.renderer.ApplyModifiedProperties();

      UnityEngine.Object[] models = this.targets;
      UnityEngine.Object[] views = this.renderer.targetObjects;

      int n = Math.Min(models.Length, views.Length);
      for (int i = 0; i < n; i++) {
        Model model = (Model)models[i];
        View view = (View)views[i];

        string err = view.Save(model);
        if (err.Length > 0) {
          Debug.Log(err, model);
        }
      }

      this.Repaint();
    }

    protected override void ResetValues() {
      this.Dispose();

      UnityEngine.Object[] models = this.targets;

      int n = models.Length;
      View ctx = null;
      var views = new View[n];

      bool fallback = false;
      for (int i = 0; i < n; i++) {
        Model model = (Model)models[i];
        views[i] = ScriptableObject.CreateInstance<View>();
        string err = views[i].Load(model);
        if (err.Length > 0) {
          Debug.Log(err, model);
          fallback = true;
          break;
        }
        if (model == this.target) {
          ctx = views[i];
        }
      }

      if (fallback) {
        for (int i = 0; i < n; i++) {
          if (views[i]) {
            DestroyImmediate(views[i]);
          }
        }
        return;
      }

      this.renderer = new SerializedObject(views, ctx);
      this.renderer.Update();

      Repaint();
      base.ResetValues();
    }

    protected override void Awake() {
      base.Awake();
      this.ResetValues();
    }

    protected virtual void OnDestroy() {
      this.Dispose();
    }

    public override void OnInspectorGUI() {
      if (this.renderer == null) {
        this.DrawDefaultInspector();
        return;
      }

#if UNITY_2019_1_OR_NEWER
      this.serializedObject.Update();
#endif

      this.DrawModel();
      this.DrawSeparator();
      this.DrawView();

#if UNITY_2019_1_OR_NEWER
      this.serializedObject.ApplyModifiedProperties();
#endif
      this.ApplyRevertGUI();
    }

    protected virtual void DrawModel() {
      SerializedProperty property = this.serializedObject.GetIterator();
      bool enterChildren = true;
      while (property.NextVisible(enterChildren)) {
        enterChildren = EditorGUILayout.PropertyField(property);
      }
    }

    protected virtual void DrawSeparator() => EditorGUILayout.Separator();

    protected virtual void DrawView() {
      if (this.renderer == null) {
        return;
      }

      SerializedProperty property = this.renderer.GetIterator();
      bool enterChildren = true;
      enterChildren = property.NextVisible(enterChildren); // skip
      while (property.NextVisible(enterChildren)) {
        enterChildren = EditorGUILayout.PropertyField(property);
      }
    }

    #endregion

    #region IDisposable

    public void Dispose() {
      if (this.renderer == null) {
        return;
      }

      foreach (View view in this.renderer.targetObjects) {
        DestroyImmediate(view);
      }
      this.renderer.Dispose();
    }

    SerializedObject renderer;

    #endregion

#endif

  }

}
