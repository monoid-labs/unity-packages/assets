﻿using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(IdentifierAttribute))]
  public sealed class IdentifierDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      if (ColorPropertyField(position, property, label)) {
        return;
      }
      EditorGUI.PropertyField(position, property, label, true);
    }

    bool ColorPropertyField(Rect position, SerializedProperty property, GUIContent label) {
      if (property.propertyType != SerializedPropertyType.String) {
        return false;
      }
      if (Identifiers.Validate(property.stringValue)) {
        return false;
      }
      Color color = GUI.color;
      GUI.color = Color.red;
      EditorGUI.PropertyField(position, property, label, true);
      GUI.color = color;
      return true;
    }
  }

}
