﻿using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets {

  [CustomPropertyDrawer(typeof(LayerAttribute))]
  public sealed class LayerAttributeEditor : PropertyDrawer {
    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label) {
      EditorGUI.BeginProperty(pos, label, prop);
      int index = prop.intValue;
      if (index > 31) {
        Debug.LogWarning("Layer index is to high '" + index + "', is set to 31.");
        index = 31;
      } else if (index < 0) {
        Debug.LogWarning("Layer index is to low '" + index + "', is set to 0");
        index = 0;
      }
      prop.intValue = EditorGUI.LayerField(pos, label, index);
      EditorGUI.EndProperty();
    }
  }

}
