# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.23] - 2021-10-13

- EnumDrawer fix
- Less `var` usage

## [1.0.22] - 2021-10-13

- DataEditor modernization follow-up

## [1.0.21] - 2021-10-13

- DataEditor modernized for Unity 2019.2+
- Applied .editorconfig formatting to various files

## [1.0.20] - 2021-09-25

- Option Attribute & Drawer

## [1.0.19] - 2021-09-23

- Option Attribute & Drawer

## [1.0.18] - 2021-06-30

- Insets & Limits Drawer Fixes (indent level)

## [1.0.17] - 2021-06-29

- Insets Attribute & Drawer

## [1.0.16] - 2021-06-29

- Limits Drawer Fixes

## [1.0.15] - 2021-05-01

- Limits Attribute & Drawer

## [1.0.14] - 2021-04-29

- EditorOnly & Asset attributes removed

## [1.0.13] - 2021-04-05

- Asset Re-serialization

## [1.0.12] - 2021-01-13

- ApplyRevertGUI
- Delayed Editor - Unapplied modifications

## [1.0.11] - 2021-01-12

- Enum Attribute & Drawer

## [1.0.10] - 2020-12-15

- EditOnly Attribute & Drawer

## [1.0.9] - 2020-12-09

- Asset Attribute & Drawer

## [1.0.8] - 2020-12-07

- EnumFlags Drawer fix
- DelayedEditor for `ScriptableObject`s (apply/revert gui)

## [1.0.7] - 2020-12-06

- Layer Attribute & Drawer
- EnumFlags Attribute & Drawer

## [1.0.6] - 2020-12-03

- Identifier Attribute & Drawer

## [1.0.5] - 2020-11-25

- Package Query Fix

## [1.0.4] - 2020-07-01

- Missing Script / Material / Shader support in Find Usage tool

## [1.0.3] - 2020-06-29

- Namespace fix
- Asset Usage Rework

## [1.0.2] - 2020-06-28

- BSD 2-Clause License
- `.editorconfig`

## [1.0.1] - 2019-09-04

- Missing-References Tool
- Namespace clean-up

## [1.0.0] - 2019-09-04

Minor code clean-up. Version to 1.0 since package seems relatively stable with no changes in a long time.

## [0.0.1-preview.1] - 2018-07-26

This is the first release.
