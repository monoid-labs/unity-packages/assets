# Asset Scripts for Unity3D

Find usages of certain _Unity3D_ project assets within others.

- Scripts on Prefabs
- Shaders used by Materials
- Materials in Prefabs and Models used by renderers

Find missing

- Scripts on Prefabs
- References in Prefabs and `ScriptableObject`s

Custom property drawers

- string as enum value
- string as option value (array of strings)
- int as unity layer

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))
