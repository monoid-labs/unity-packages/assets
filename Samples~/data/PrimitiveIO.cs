using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

using UnityEngine;

namespace Monoid.Unity.Assets.Samples {

  public static class PrimitiveIO {

    public static string Load(string path, out Primitive[] primitives) {
      string text;
      try {
        text = File.ReadAllText(path);
      } catch (IOException e) {
        primitives = new Primitive[0];
        return $"error reading file: {e.Message}";
      }
      return Decode(text, out primitives);
    }

    public static string Save(string path, Primitive[] primitives) {
      string text = Encode(primitives);
      try {
        File.WriteAllText(path, text);
      } catch (Exception e) {
        return $"error writing file: {e.Message}";
      }
      return string.Empty;
    }


    public static string Decode(string text, out Primitive[] primitives) {
      var list = new List<Primitive>();

      using (StringReader sr = new StringReader(text)) {
        string line;
        int i = 0;
        while ((line = sr.ReadLine()) != null) {
          i++;

          line = CleanLine(line);
          if (line.Length == 0) {
            continue;
          }

          Primitive entry = Primitive.Default;
          string err = Decode(line, ref entry);
          if (err.Length != 0) {
            primitives = new Primitive[0];
            return $"line[{i}] {err}";
          }

          list.Add(entry);
        }
      }

      primitives = list.ToArray();
      return string.Empty;
    }

    static string Decode(string line, ref Primitive p) {
      string[] tokens = line.Split();
      if (tokens.Length < 11) {
        return $"expected 11 tokens but identified {tokens.Length}: `{string.Join("` `", tokens)}`";
      }

      if (!Enum.TryParse<PrimitiveType>(tokens[0], true, out p.type)) {
        return $"invalid primitive type `{tokens[0]}`";
      }

      bool ok = true;
      ok &= ReadFloat(tokens[1], out p.position.x);
      ok &= ReadFloat(tokens[2], out p.position.y);
      ok &= ReadFloat(tokens[3], out p.position.z);
      ok &= ReadFloat(tokens[4], out p.rotation.x);
      ok &= ReadFloat(tokens[5], out p.rotation.y);
      ok &= ReadFloat(tokens[6], out p.rotation.z);
      ok &= ReadFloat(tokens[7], out p.scale.x);
      ok &= ReadFloat(tokens[8], out p.scale.y);
      ok &= ReadFloat(tokens[9], out p.scale.z);
      if (!ok) {
        return $"invalid position|rotation|scale xyz";
      }

      if (!ColorUtility.TryParseHtmlString("#" + tokens[10], out p.color)) {
        return $"invalid color `{tokens[10]}`";
      }

      return string.Empty;
    }

    public static string Encode(Primitive[] primitives) {
      var sb = new StringBuilder();
      sb.AppendLine("# TYPE tx ty tz rx ry rz sx sy sz");
      sb.AppendLine();

      foreach (Primitive p in primitives) {
        string type = p.type.ToString().ToLowerInvariant();
        string px = WriteFloat(p.position.x);
        string py = WriteFloat(p.position.y);
        string pz = WriteFloat(p.position.z);
        string rx = WriteFloat(p.rotation.x);
        string ry = WriteFloat(p.rotation.y);
        string rz = WriteFloat(p.rotation.z);
        string sx = WriteFloat(p.scale.x);
        string sy = WriteFloat(p.scale.y);
        string sz = WriteFloat(p.scale.z);
        string color = ColorUtility.ToHtmlStringRGBA(p.color);
        sb.AppendLine($"{type} {px} {py} {pz} {rx} {ry} {rz} {sx} {sy} {sz} {color}");
      }

      sb.AppendLine();

      return sb.ToString();
    }


    static string CleanLine(string line) {
      // remove comment
      int comment = line.IndexOf('#');
      if (comment >= 0) {
        line = line.Substring(0, comment);
      }
      // remove leading and trailing whitespace
      line = line.Trim();
      // return cleaned line string
      return line;
    }

    static bool ReadFloat(string text, out float value) => float.TryParse(text, NumberStyles.Float, CultureInfo.InvariantCulture, out value);
    static string WriteFloat(float value) => value.ToString(CultureInfo.InvariantCulture);

  }

}
