using UnityEngine;
using UnityEditor;

namespace Monoid.Unity.Assets.Samples {

  [CustomEditor(typeof(PrimitivesImporter)), CanEditMultipleObjects]
  public sealed class PrimitivesImporterEditor : DataEditor<PrimitivesImporter, PrimitivesImporterView> {
  }

}
