using System;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Assets.Samples {

  [ScriptedImporter(1, ".primitives", 23)]
  public sealed class PrimitivesImporter : ScriptedImporter {

    [Min(0)] public float globalScale = 1.0f;

    public override void OnImportAsset(AssetImportContext ctx) {
      string path = ctx.assetPath;
      string dir = Path.GetDirectoryName(path);
      string name = Path.GetFileNameWithoutExtension(path);

      Primitive[] primitives;
      string err = PrimitiveIO.Load(path, out primitives);
      if (err.Length > 0) {
        Debug.LogError($"Parsing error `{path}`: {err}", this);
        return;
      }

      var materials = new Dictionary<string, Material>(primitives.Length);

      var scene = new GameObject(name);
      foreach (Primitive p in primitives) {
        Transform t = GameObject.CreatePrimitive(p.type).transform;
        t.SetParent(scene.transform);
        t.localPosition = p.position * this.globalScale; ;
        t.localRotation = Quaternion.Euler(p.rotation);
        t.localScale = p.scale * this.globalScale;
        Renderer renderer = t.GetComponent<Renderer>();
        if (renderer) {
          Material material;
          string key = ColorUtility.ToHtmlStringRGBA(p.color);
          if (!materials.TryGetValue(key, out material)) {
            material = new Material(renderer.sharedMaterial);
            material.name = key;
            materials.Add(key, material);
            material.color = p.color;
            ctx.AddObjectToAsset(key, material);
          }
          renderer.sharedMaterial = material;
        }
      }

      ctx.AddObjectToAsset(name, scene);
      ctx.SetMainObject(scene);
    }

  }

}
