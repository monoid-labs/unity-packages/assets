using System;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using UnityEditor;
#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace Monoid.Unity.Assets.Samples {

  public sealed class PrimitivesImporterView : ScriptableObject, PrimitivesImporterEditor.IView {
    public string Load(PrimitivesImporter target)
        => PrimitiveIO.Load(target.assetPath, out this.primitives);

    public string Save(PrimitivesImporter target)
        => PrimitiveIO.Save(target.assetPath, this.primitives);

    public Primitive[] primitives = { };
  }

}
