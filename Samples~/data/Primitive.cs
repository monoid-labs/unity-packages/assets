using System;

using UnityEngine;

namespace Monoid.Unity.Assets.Samples {

  [Serializable]
  public struct Primitive {
    public static readonly Primitive Default = new Primitive {
      type = PrimitiveType.Cube,
      position = Vector3.zero, rotation = Vector3.zero, scale = Vector3.one,
      color = Color.white,
    };

    public PrimitiveType type;
    public Vector3 position, rotation, scale;
    public Color color;
  }

}
