using System;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public sealed class InsetsAttribute : PropertyAttribute {
    public readonly float range;

    public InsetsAttribute() : this(1) { }

    public InsetsAttribute(float range) {
      if (range <= 0) {
        throw new ArgumentException($"Insets range must be positive but was {range}");
      }
      this.range = range;
    }
  }

}
