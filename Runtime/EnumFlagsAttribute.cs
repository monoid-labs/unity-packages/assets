﻿using UnityEngine;

namespace Monoid.Unity.Assets {

  public sealed class EnumFlagsAttribute : PropertyAttribute { }

}
