using System;
using UnityEngine;

namespace Monoid.Unity.Assets {

  public sealed class OptionAttribute : PropertyAttribute {
    public readonly string[] labels;

    public OptionAttribute(params string[] labels) {
      this.labels = (string[]) labels;
    }
  }

}
